//// Esther Park, CSE 02
//// HW 04, 9/24/2018
/////////
import java.util.Scanner; ///imports scanner
public class CrapsIf { //starts container
  public static void main (String[] args){ //necessary for every program
    Scanner myScanner = new Scanner (System.in); //constructs scanner
   
    int diceRoll1 = 0;  //declares diceRoll1
    int diceRoll2 = 0;  //declares diceRoll2
   
    System.out.print("If you would like to select dice values, please type 'yes'. "); //asks user to types yes if they want manually selected numbers
    System.out.print("If you would like randomly generated numbers, please type 'no'. "); //asks user to type no if they want random numbers
    String result = myScanner.nextLine(); //scans for input
    
    if (result.equals("yes")) { //if else statement starts
      System.out.println("Please type your 2 numbers between 1 and 6 separated by a space key");  //prints for user to select numbers
      diceRoll1 = myScanner.nextInt(); //scans for user input
      diceRoll2 = myScanner.nextInt(); //scans for user input
      
      System.out.println("You selected " + diceRoll1 + " and " + diceRoll2); //prints out results
      
    if (diceRoll1 < 1 || diceRoll1 > 6 || diceRoll2 < 1 || diceRoll2 > 6){
      System.out.println("Invalid numbers were entered"); //prints error message if invalid numbers were selected
    }
      
    else if (diceRoll1 == 1 && diceRoll2 ==1) {System.out.println("Snake Eyes"); //else if statement for snake eyes
   }
    else if (diceRoll1 == 2 && diceRoll2 == 1 || diceRoll1 == 1 && diceRoll2 == 2) {System.out.println("Ace Duece"); //else if statement for ace duece
   } 
    else if (diceRoll1 == 2 && diceRoll2 == 2) {System.out.println("Hard Four"); ////else if statement for hard four
   } 
    else if (diceRoll1 == 3 && diceRoll2 == 1 || diceRoll1 == 1 && diceRoll2 == 3) {System.out.println("Easy Four"); ////else if statement for easy four
   } 
   else if (diceRoll1 == 3 && diceRoll2 == 2 || diceRoll1 == 2 && diceRoll2 == 3 || diceRoll1 == 4 && diceRoll2 == 1 || diceRoll1 == 1 && diceRoll2 == 4) {System.out.println("Fever Five");
   }  
    else if (diceRoll1 == 3 && diceRoll2 == 3) {System.out.println("Hard Six");
   } 
    else if (diceRoll1 == 4 && diceRoll2 == 2 || diceRoll1 == 2 && diceRoll2 == 4 || diceRoll1 == 5 && diceRoll2 == 1 || diceRoll1 == 1 && diceRoll2 == 5) {System.out.println("Easy Six");
   }
    else if (diceRoll1 == 4 && diceRoll2 == 3 || diceRoll1 == 3 && diceRoll2 == 4 || diceRoll1 == 5 && diceRoll2 == 2 || diceRoll1 == 2 && diceRoll2 == 5 || diceRoll1 == 6 && diceRoll2 == 1 || diceRoll1 == 1 && diceRoll2 == 6) {System.out.println("Seven Out");
   } 
    else if (diceRoll1 == 4 && diceRoll2 == 4) {System.out.println("Hard Eight");
   }
    else if (diceRoll1 == 5 && diceRoll2 == 3 || diceRoll1 == 3 && diceRoll2 == 5 || diceRoll1 == 6 && diceRoll2 == 2 || diceRoll1 == 2 && diceRoll2 == 6) {System.out.println("Easy Eight");
   }
    else if (diceRoll1 == 5 && diceRoll2 == 4 || diceRoll1 == 4 && diceRoll2 == 5 || diceRoll1 == 6 && diceRoll2 == 3 || diceRoll1 == 3 && diceRoll2 == 6) {System.out.println("Nine");
   }
    else if (diceRoll1 == 5 && diceRoll2 == 5) {System.out.println("Hard Ten");
   }
    else if (diceRoll1 == 6 && diceRoll2 == 4 || diceRoll1 == 4 && diceRoll2 == 6) {System.out.println("Easy Ten");
   }
    else if ((diceRoll1 == 6 && diceRoll2 == 5) || (diceRoll1 == 5 && diceRoll2 == 6)) {System.out.println("Yo-leven");
   }
    else if (diceRoll1 == 6 && diceRoll2 == 6) {System.out.println("Box Cars");
   }
 }
     
    
    else if (result.equals("no")){ //starts diff scenerio for random numbers
      diceRoll1 = (int) (Math.random() * 6) + 1; //generated random numbers
      diceRoll2 = (int) (Math.random() * 6) + 1; //generates random numbers
      
      System.out.println("You rolled " + diceRoll1 + " + " + diceRoll2); //prints random numbers selected
    }
    else {
      System.out.println("Invalid answer. Please restart the program."); //error message printed
      return;
    }
     if (diceRoll1 == 1 && diceRoll2 ==1) {System.out.println("Snake Eyes"); //snake eyes scenerio 
   }
    else if (diceRoll1 == 2 && diceRoll2 == 1 || diceRoll1 == 1 && diceRoll2 == 2) {System.out.println("Ace Duece");
   } 
    else if (diceRoll1 == 2 && diceRoll2 == 2) {System.out.println("Hard Four");
   } 
    else if (diceRoll1 == 3 && diceRoll2 == 1 || diceRoll1 == 1 && diceRoll2 == 3) {System.out.println("Easy Four");
   } 
   else if (diceRoll1 == 3 && diceRoll2 == 2 || diceRoll1 == 2 && diceRoll2 == 3 || diceRoll1 == 4 && diceRoll2 == 1 || diceRoll1 == 1 && diceRoll2 == 4) {System.out.println("Fever Five");
   }  
    else if (diceRoll1 == 3 && diceRoll2 == 3) {System.out.println("Hard Six");
   } 
    else if (diceRoll1 == 4 && diceRoll2 == 2 || diceRoll1 == 2 && diceRoll2 == 4 || diceRoll1 == 5 && diceRoll2 == 1 || diceRoll1 == 1 && diceRoll2 == 5) {System.out.println("Easy Six");
   }
    else if (diceRoll1 == 4 && diceRoll2 == 3 || diceRoll1 == 3 && diceRoll2 == 4 || diceRoll1 == 5 && diceRoll2 == 2 || diceRoll1 == 2 && diceRoll2 == 5 || diceRoll1 == 6 && diceRoll2 == 1 || diceRoll1 == 1 && diceRoll2 == 6) {System.out.println("Seven Out");
   } 
    else if (diceRoll1 == 4 && diceRoll2 == 4) {System.out.println("Hard Eight");
   }
    else if (diceRoll1 == 5 && diceRoll2 == 3 || diceRoll1 == 3 && diceRoll2 == 5 || diceRoll1 == 6 && diceRoll2 == 2 || diceRoll1 == 2 && diceRoll2 == 6) {System.out.println("Easy Eight");
   }
    else if (diceRoll1 == 5 && diceRoll2 == 4 || diceRoll1 == 4 && diceRoll2 == 5 || diceRoll1 == 6 && diceRoll2 == 3 || diceRoll1 == 3 && diceRoll2 == 6) {System.out.println("Nine");
   }
    else if (diceRoll1 == 5 && diceRoll2 == 5) {System.out.println("Hard Ten");
   }
    else if (diceRoll1 == 6 && diceRoll2 == 4 || diceRoll1 == 4 && diceRoll2 == 6) {System.out.println("Easy Ten");
   }
    else if (diceRoll1 == 5 && diceRoll2 == 6 || diceRoll1 == 6 && diceRoll2 == 5) {System.out.println("Yo-leven");
   }
    else if (diceRoll1 == 6 && diceRoll2 == 6) {System.out.println("Box Cars");
   }
 }
  
  }
 
