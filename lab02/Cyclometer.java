//// Esther Park, Lab 02
/// CSE 002 - 310
//////
public class Cyclometer { /// main method for java program
  public static void main (String[] args) {
    int secTrip1 = 480; // declares time for trip one
    int secTrip2 = 3220; // declares time for trip two
    int countsTrip1 = 1561; // declares counts for trip one
    int countsTrip2 = 9037; // declares counts for trip two
    
    double wheelDiameter = 27.0; // declares wheel diameter
    double PI = 3.14159; // inputs pi value
    int feetPerMile = 5280; // inputs feet per mile
    int inchesPerFoot = 12; // inputs inches per foot
    int secondsPerMinute = 60; // inputs seconds per minute
    
    System.out.println("Trip 1 took " + (secTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts."); // prints trip 1 took x minutes and x countsTrip1
    System.out.println("Trip 2 took " + (secTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts."); //prints trip2 mintues and counts
    
    double distanceTrip1 = countsTrip1 * wheelDiameter * PI / inchesPerFoot / feetPerMile; //calculates distance of trip 1
    double distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; // calculates distance of trip 2
    double totalDistance = distanceTrip1 + distanceTrip2; // calculates total distance
    
    System.out.println("Trip 1 was " + distanceTrip1 + " miles"); // prints trip 1 in miles
    System.out.println("Trip2 was " + distanceTrip2 + " miles"); // prints trip 2 in miles
    System.out.println("The total distance was " + totalDistance + " miles"); //prints total distance
    
  } //end of main method
  
} // end of class
