//// Esther Park
//////// CSE 002 - 310
//// HW 08 - November 13, 2018

import java.util.Scanner; ///imports scanner
public class Shuffling{ //necessary for every program
  public static void printArray(String[] list){ // pintArray method is used to use the card variable declared in the main method to print strings
    for (int i=0;i<list.length;i++){ //for loop to go through the 52 spaces in the array 
      System.out.print(list[i] + " "); // prints the array elements with spaces
    }
    System.out.println(); //used to continue printing on next line
  }
  public static void shuffle(String[] list){ ///new method called shuffle used to shuffle cards
    for (int i=0; i<list.length; i++){ //loops through the array to shuffle
      int numRandom = (int)(Math.random()*52); //generates random number
      String tempValue = list[i]; //assigns temporary value in order to swap the list[i] and 
      list[i] = list[numRandom]; //swaps the positions to make sure it is shuffling
      list[numRandom] = tempValue; //uses temp value to retain original variable value
    }
  }
  public static String[] getHand(String[] list, int index, int numCards){ //method called getHand
    if (numCards > (index+1)){ //if the number of cards asked for is greater than the number of cards left in remaining deck, shuffle
      shuffle(list); //call shuffle method to shuffle cards
      index = 51; //now the starting point or index to deal cards will be back to 51 
     }
    String hand[] = new String[numCards]; //creates new array called hand
    for(int i=0; i<=numCards-1;i++){ //for loop goes through the 5 cards
      hand[i] = list[index-i]; //hand [i] is now the position in the array called list where i number(s) is subtracted from previous index point
      }
     return hand; //returns the values in hand array
  }
  public static void main (String[] args){
    Scanner scan = new Scanner(System.in); //enters new scanner
    String[] suitNames = {"C","H","S","D"}; //assigns elements in array called suitnames
    String[] rankNames = {"2", "3", "4", "5", "6", "7","8","9","10","J","Q","K","A"}; //assings elements in array called ranknames
    String[] cards = new String[52]; //creates new array with length 52
    String[] hand = new String[5]; //creates new array with length 5
    int numCards = 5;
    int again = 1;
    int index = 51;
    for (int i=0; i<52;i++){ //for loop going through the 51 cards and using modulus to assing the ranknames and suitnames
      cards[i]=rankNames[i%13]+suitNames[i/13];
      System.out.print(cards[i]+" "); //print cards value w suitname and ranknames
    }
   System.out.println();
   printArray(cards);  //calls method printarray
   shuffle(cards); //calls shuffle method
   printArray(cards); ///calls printarray method

    while(again == 1){ //while user ansers 1
      hand = getHand(cards,index,numCards); //cals method getHand
      printArray(hand);
      index = index - numCards; //index refers to the position the next hand will be dealt starting from
      System.out.println("Enter a 1 if you want another hand drawn"); //asks user to enter for another hand
      again = scan.nextInt(); //scans for user input
    }
  } // end of main method
} //end of class