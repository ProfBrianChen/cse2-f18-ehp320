import java.util.Scanner;
// Lab 03, Esther Park
//
//
public class Check{
  // necessary every time for Java program
  public static void  main (String[] args)  {
    Scanner myScanner = new Scanner (System.in); //accept input and call scanner constructor
    System.out.print("Enter the original cost of the check in the form xx.xx: "); // promts user to input
    double checkCost = myScanner.nextDouble (); //accept user input
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx) : ");
    double tipPercent = myScanner.nextDouble() ;
    tipPercent /=100; //converts percentage into decimal value
    System.out.print ("Enter the number of people who went out to dinner: "); //promts user for number of people that went to dinner
    int numPeople = myScanner.nextInt() ; //accept input
    double totalCost;
    double costPerPerson;
    int dollars, //whole dollar of cost
        dimes, pennies; //for strong digits
            // to the right of the decimal point
            // for the cost$
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople; // get the whole amount, dropping decimal fraction
    dollars= (int) costPerPerson;
    // get dimes amount, e.g.,
    // (int) ( 6.73 * 10) % 10 -> 67 % 10 -> 7
    // where the % (mod) operator returns the remainder
    // after the division:   583%100 -> 83, 27%5 -> 2
    dimes=(int)(costPerPerson * 10) % 10;
    pennies=(int) (costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
    
  } // end of main method
} // end of class
