////////
//// CSE 002 HW3 Pyramid
//// Esther Park
import java.util.Scanner; // improts scanner class
public class Pyramid{ //starts program class container
  public static void main(String args []){
    Scanner myScanner = new Scanner (System.in); // creates scanner object
    System.out.println("The square side of the pyramid is (input length): ");  //promtes user to enter side length
    double length = myScanner.nextDouble(); //scans for length value
    System.out.println("The height of the pyramid is (input height): "); //prompts user to enter height
    double height = myScanner.nextDouble(); //scans for height
    double volume = (Math.pow(length, 2.0)) * height / 3; //calculates volume for square base
    System.out.println("The volume inside the pyramid is: " + volume); //print volume
    
  }
  
}
