////////
// CSE 002 HW3 Convert
//// Esther Park
import java.util.Scanner; //import scanner class
public class Convert { //necessary to create program
  public static void main (String[] args){
    Scanner myScanner = new Scanner (System.in); // declares new scanner
    System.out.print("Enter the affected area in acres: "); //prompts user to enter area in acres
    double area = myScanner.nextDouble(); ///scans for area
    System.out.print("Enter the rainfall in the affected area: "); //prompts user to enter rainfall in inches
    double rainfall = myScanner.nextDouble(); //scans for rainfall
    int inchesPerMile = 63360; //converts from inches to cubic miles
    int acresPerSquareMile = 640; //converts from acres to square miles
    double rainVolume = (area/acresPerSquareMile) * (rainfall/inchesPerMile); // calculates rain volume
    System.out.println(rainVolume + " cubic miles."); //prints rain volume in cubic miles
    
    
  }
}