///// Esther Park
/// CSE002 - HW 26
// November 26, 2018

import java.util.Scanner; //imports scanner
import java.util.Random; //imports random
public class CSE2Linear{ //necessary for every program
  public static void linearSearch(int studentGrades[], int searchResult){ //linearSearch method used to find the student's grade using a linear search
    for (int i=1; i<studentGrades.length; i++){ //uses for loop through the array size 
      if (studentGrades[i] == searchResult){ //if the element in the array is equal to the user input, proceed
        System.out.print(searchResult + " was found with "+ (i+1)+ " iterations."); //prints out if the user input was found and with how many iterations
        return;
      }
    }
    System.out.print("Error: " + searchResult + " was not found."); //if user input was not found, error message with be printed
  }
  public static void binarySearch(int[] studentGrades, int leftValue, int rightValue, int searchResult){ //binarySeach method is used to find the user input using a binary search
    int i=1; //initialize i starting with 1 iteration
    while(leftValue<=rightValue){ // while the leftmost value is less than the right value proceed
      int middleIndex = leftValue + (rightValue-leftValue)/2; //middleIndex the mid point 
      if (searchResult==studentGrades[middleIndex]){ //if the user input is the middle value in the studentGrades array
        System.out.println(searchResult + " was found with " + i +" interations."); //prints the result and number or iterations took
        return;
      }
      else if (searchResult>studentGrades[middleIndex]){ //if the user input is greater than the mipoint within the array, assign a new bound/leftvalue
        leftValue=middleIndex+1; //add one since array starts at 0
      }
      else if (searchResult< studentGrades[middleIndex]){//if the user input is less than the midpoint within the array, assign a new middle point
        rightValue=middleIndex-1; //subtract one since array starts at 0
      }
      i++; //number of iterations counter
    }
    System.out.println("Error: " + searchResult+ " was never found. Scrambled: "); //error message
  }
  public static void scramble(int[] studentGrades){ //scramble metho
    for (int i=0; i<studentGrades.length; i++){ //for loop for scrambling
      int enteredGrade = (int)(Math.random()*studentGrades.length); //random number within array size
      int tempValue = studentGrades[enteredGrade]; //assign temporary value to make sure the array was randomized
      studentGrades[enteredGrade] = studentGrades[i]; 
      studentGrades[i] = tempValue; //way to swap without losing initial value
    }
  }
  public static void main (String[] args){ //main method
    Scanner myScanner = new Scanner(System.in);
    int[] studentGrades = new int[15];   //new array with length 15
    boolean validInput = false;
      while(!validInput){ //while true
        System.out.println("Enter 15 increasing integers to find CSE final grades.");
        for(int i=0; i<studentGrades.length;i++){ //loops through array 
          if (myScanner.hasNextInt()){ //checks if integer
            studentGrades[i] = myScanner.nextInt(); //assigns element in array
              if (studentGrades[i]<0 || studentGrades[i]>100){ //if within range
                System.out.println("Your input was out of the range 0-100, please enter valid integer.");
                myScanner.nextLine();
                break;
              } // end of nested if statement
               if(i!=0 && studentGrades[i]<studentGrades[i-1]){ //if second value is equal to or 
                System.out.println("Your new input is not greater than or equal to your last input, please try again.");
                myScanner.nextLine();
                break;
              } //end of nested if statement 2
        } //end of outer if statement
       else { //if input is not an integer
         System.out.println("Your input was not an integer, please enter a valid input.");
         myScanner.nextLine();
         break;
        } 
        if (i==studentGrades.length-1){ //if the loop gets to the end of array and still within loop, then the value is correct
          validInput = true;
        }
    } //end of while loop
  }// end of for loop
  for (int m: studentGrades){ //this is how you make a for loop for arrays
    System.out.print(m+" "); //make sure to print w spaces
  }
   System.out.println("Please enter a grade you would like to search for"); //prints question for user input
   int searchResult = myScanner.nextInt(); //must be integer, no need to check
   binarySearch(studentGrades, 0, 15, searchResult); //calls binarySearch method
   scramble(studentGrades); //calls scramble method
   for (int m: studentGrades){ //array for loop
     System.out.print(m+" ");
   }
   System.out.println("Enter a number you'd like to search linearly");
   searchResult = myScanner.nextInt(); //must be integer
   linearSearch(studentGrades, searchResult); //calls linearSearch method
  }//end of main method
} //end of class