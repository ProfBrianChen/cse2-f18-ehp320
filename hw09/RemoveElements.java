//// Esther Park
/// CSE002 - HW9
/// November 27, 2018

import java.util.Scanner; //import scanner
public class RemoveElements{ //necessary for every program
  
  public static void main(String [] arg){ //main method
	  Scanner scan=new Scanner(System.in); //new scanner
    int num[]=new int[10]; //new array called num with length 10
    int newArray1[];
    int newArray2[];
    int index,target;
	  String answer="";
	   do{
  	  System.out.print("Random input 10 ints [0-9]");
  	  num = randomInput(); //calls method randomInput with no input
  	  String out = "The original array is:";
  	  out += listArray(num); //calls method listArray with array input
  	  System.out.println(out); //prints out array
 
  	  System.out.print("Enter the index ");
  	  index = scan.nextInt(); //scans for user input 
  	  newArray1 = delete(num,index); // calls delete method to remove the element in the index that the user has chosen
  	  String out1="The output array is ";
  	  out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	  System.out.println(out1); //prints new array
 
      System.out.print("Enter the target value "); //prompts user to enter a new value 
  	  target = scan.nextInt(); //scans for target value
  	  newArray2 = remove(num,target); //calls remove method to get rid of elements chosen by user
  	  String out2="The output array is ";
  	  out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	  System.out.println(out2); //prints array
  	 
  	  System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-"); //asks user if they want to go again
  	  answer=scan.next();
	  } while(answer.equals("Y") || answer.equals("y"));
  } //end of main method
 
  public static String listArray(int[] num){ //method called listArray
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} "; //returns array with spaces and commas
	return out;
  } // end of listArray Method
  
  public static int[] randomInput(){ //method called randomInput to assign random numbers to each array index 
    int[] randomArray = new int[10];
    for (int i=0; i<=9; i++){ //for loop to assign new elements
      randomArray[i] = (int)(Math.random()*9);  //random generator
    }
    return randomArray; //returns new array
  } //end of randomInput
  
  public static int[] delete(int[] list, int pos){ //delete method used to delete the element in the specified index
    int[] anArray = new int[list.length-1];
    for (int i=0; i<pos; i++){ /// uses for loop to copy same element into array
     anArray[i] = list[i]; //copies array until the same element appears
    }
    for(int i=pos;i<list.length-1; i++){ //when the specified position appears, it skips and the rest of the array shifts down
      anArray[i]=list[i+1];
    }
    return anArray;
  }//end of method
  
  public static int[] remove(int[] list, int target){ //new method called remove
   int p=0;
    for (int i=0; i<list.length; i++){ //uses for loop to find where the speficied element appears and replaces with the one after
      if(list[i] == target){
        p++;
        for(int j=i; j<list.length-1; j++){
          list[j] = list[j+1]; // replaces element with the one after
       }
      }
    } 
   int[] someArray = new int[list.length-p]; //creating new array with old array length - # of deleted elements
     for (int i=0; i<list.length-p; i++){ /// uses for loop to copy array into new array
      someArray[i] = list[i]; // copying old into new 
  } 
    return someArray; //return statement
}//end of method
}//end of class
