////// Esther Park
//// CSE Lab 06 Patterns
///// October 22, 2018

import java.util.Scanner;


public class PatternA {
  public static void main (String[] args) {
    Scanner myScanner = new Scanner(System.in);
 
 int numInput;
    numInput = 0;
 
while (numInput >= 0) {  
 System.out.println("Input integer between 1-10");
    boolean correct = myScanner.hasNextInt();
    while (correct==false) {
      System.out.println("Invalid input, input integer between 1-10");
      myScanner.next();
      correct = myScanner.hasNextInt();
    }
    while (numInput >= 0){
   numInput = myScanner.nextInt();
  for (int ii = 1; ii <= numInput; ii++) {
    for (int mm = 1; mm <= ii; mm++){
      System.out.print(" " + mm );
    }
    System.out.println();
     }  
    }
   }
  }
 }
    
   
       