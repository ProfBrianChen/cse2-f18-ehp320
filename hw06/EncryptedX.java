///// Esther Park
/// CSE 002 - HW 06 Encrypted X
////// October 22, 2018

import java.util.Scanner;
public class EncryptedX {
  public static void main (String[] args) { //always necessary
    Scanner myScanner = new Scanner(System.in); //input scanner
    
    int numSize; // initialize the user input size variable
    numSize = 0;
    
  
    System.out.println("Input an integer between 0-100"); // asks user to input an integer
    boolean result = myScanner.hasNextInt(); // checks input
    while (result==false) { // uses boolean to check if input was false and loops until user inputs valid answer
         System.out.println("Invalid input, enter an integer between 0-100"); // asks user to reenter
          myScanner.next();
          result = myScanner.hasNextInt();
    }
    numSize = myScanner.nextInt();
    
    for (int i = 1; i <= numSize; i++) { // sets outer for loop dependent on user input
        for (int m = 1; m <= numSize; m++){ // inner for loops determines what pattern is printed
             if (i==m || m==numSize + 1 - i){
               System.out.print(" "); // prints spaces if satisfies if statement
                }
                  else {
                    System.out.print("*");
                  }
               }
    System.out.println(); 
     
     } // end of outer for loop
  } // end of main method
} // end of class

