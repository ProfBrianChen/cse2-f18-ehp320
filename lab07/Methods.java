///// Esther Park
/// CSE 002 - Lab 07
/////// October 25, 2018

import java.util.Random; /// used to import random generator
import java.util.Scanner; // used to import scanner
public class Methods { // necessary for all code within class
public static int numRandom(){ // method used to generate random number
Random randomGenerator = new Random (); // new random generator
    int RandomInt = randomGenerator.nextInt(10);   /// initialize RandomInt variable to generate numbers 1-10
    return RandomInt; // returns the random number
}
public static String printAdj (){ // new method to generate random adjective
    switch (numRandom()){ // switch statement used to incorporate the random number for a random adjective
         case 0: 
           return "happy";
         case 1: 
           return "tired";
         case 2: 
           return "sad";
         case 3: 
           return "vivid";
         case 4: 
           return "colorful";
         case 5: 
           return "gloomy";
         case 6: 
           return "bright";
         case 7: 
           return "sleepy";
         case 8: 
           return "great";
         case 9: 
           return "confused";
         default:
           return "Error";   
       }  
  }
  public static String printSubject (){ // new method for subjects
    switch (numRandom()){ /// switch statement returning strings of subject nouns
         case 0: 
           return "dog";
         case 1: 
           return "frog";
         case 2: 
           return "cat";
         case 3: 
           return "bird";
         case 4: 
           return "fish";
         case 5: 
           return "girl";
         case 6: 
           return "airplane";
         case 7: 
           return "boy";
         case 8: 
           return "bus";
         case 9: 
           return "mom";
         default:
           return "Error";
      }
  }
public static String printVerbs (){
  switch (numRandom()){ /// switch statement returning strings of verbs
         case 0: 
           return "ran";
         case 1: 
           return "jumped";
         case 2: 
           return "ate";
         case 3: 
           return "crashed";
         case 4: 
           return "drove";
         case 5: 
           return "skipped";
         case 6: 
           return "flew";
         case 7: 
           return "sang";
         case 8: 
           return "shaked";
         case 9: 
           return "loved";
         default:
           return "Error";
      } 
  }
public static String printObject (){
  switch (numRandom()){ /// switch statement returning strings of object nouns
         case 0: 
           return "track";
         case 1: 
           return "rock";
         case 2: 
           return "cookies";
         case 3: 
           return "tree";
         case 4: 
           return "vehicle";
         case 5: 
           return "class";
         case 6: 
           return "sky";
         case 7: 
           return "song";
         case 8: 
           return "world";
         case 9: 
           return "music";
         default:
           return "Error";
       }
   }
public static String Thesis(){ // new method used to put the first sentence together
  String printSub = printSubject(); // calls printSubject method to return as string
  String genSentence = "The " + printAdj() + " " + printSub + " " + printVerbs() 
+ " the " + printAdj() + " " + printObject() + "."; // puts sentence together by calling other methods stated above
  System.out.println(genSentence); // prints the given sentence
  return printSub; // returns printSub to use in the next sentence
  } // end of thesis method
  
public static void supportSentence(String printSub){ // new method for action sentence
  String wordNoun = "";
  switch ((int)(Math.random()*2)){ // random generator to rotate between using "it" and the original noun
    case 0:
      wordNoun = "It"; 
      break;
    case 1:
      wordNoun = "The " + printSub;
      break;
  } // end of switch
  String putTogether = wordNoun + " " + printVerbs() + " the " + printAdj() + " " + printObject() + ".";
  System.out.println(putTogether); //prints the action sentence
} //end of method 
  
public static void Conclusion(String printSub){ // new method for conclusion sentence
  String putAll = "That " + printSub + " " + printVerbs() + " its " + printObject() + "!";
  System.out.println(putAll); // prints conlcusion sentence
} // end of conclusion method

public static void main (String[] args) { //always necessary main method
    Scanner myScanner = new Scanner(System.in); // uses scanner for user input
    String userInput = "yes"; //initialize userInput variable
      while (userInput.equals("yes")){ // if the user enters yes go into loop
        String printSub = Thesis(); // printSub is now a variable for calling the thesis method
        for (int i=0; i<numRandom(); i++){
          supportSentence(printSub); // calliung supportSentence method but using printSub to use the same subject
        } // end of for loop
        Conclusion(printSub); // print conclusion using same subject
        System.out.println("If you would like to generate another sentence, type 'yes'"); // asks user if they want to generate a new thesis sentence
       userInput = myScanner.next();
      
      } // end of while statement
    } // end of main method
  } // end of methods class
     
   
  