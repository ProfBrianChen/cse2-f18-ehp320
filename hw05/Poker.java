/////// Esther Park
/// HW 05, October 8, 2018
/////// CSE 002

import java.util.Scanner;


public class Poker {
  public static void main (String[] args) { //always necessary
    Scanner myScanner = new Scanner(System.in); //input scanner
    
    int genTimes; //initialize number of times generated
    
    System.out.println("How many times would you like to generate hands?"); // asks user how many they want to generate
    boolean result = myScanner.hasNextInt(); // checks input
    while (result==false) { // uses boolean to check if input was false and loops until user inputs valid answer
      System.out.println("Invalid input, enter number of times you would like to generate hands"); // asks user how many times
      myScanner.next(); // checks for answer
      result = myScanner.hasNextInt(); 
    }
    genTimes = myScanner.nextInt();
    int pairF=0; // one pair
    int twoPairs=0; // two pairs
    int tripleF=0; // three of a kind
    int fourF=0; // four of a kind
    int fullHouse=0; // full house
    /// must be initialized before for loop
    
      for (int i = 1; i <= genTimes;i++) { ///large for loop for number of generator
        int cardNum1 = (int)(Math.random()*52)+1; // generartes random number
        String card1="";
        
        int cardNum2 = (int)(Math.random()*52)+1;
        while (cardNum1 == cardNum2){ // regenerates random number if card 1 and card 2 are the same
          cardNum2 = (int)(Math.random()*52)+1;
        }
        int cardNum3 = (int)(Math.random()*52)+1;
        while (cardNum3==cardNum1 | cardNum3==cardNum2){ // regenerates random number if card 2 and card 1or3 are the same
          cardNum3 = (int)(Math.random()*52)+1;
        }
        int cardNum4 = (int)(Math.random()*52)+1;
        while (cardNum4==cardNum1 | cardNum4==cardNum2 | cardNum4==cardNum3){
          cardNum4 = (int)(Math.random()*52)+1;
        }
        int cardNum5 = (int)(Math.random()*52)+1;
         while (cardNum5==cardNum1 | cardNum5==cardNum2 | cardNum5==cardNum3 | cardNum5==cardNum4){
          cardNum4 = (int)(Math.random()*52)+1;
        }
        int c1=cardNum1%13; // use modulus to determine if king/queen/ace/jack
        int c2=cardNum2%13;
        int c3=cardNum3%13;
        int c4=cardNum4%13;
        int c5=cardNum5%13;
        int pair=0;
        int triple=0;
        int bomb=0;
        int check=0;
     for (int ii=0;ii<13;ii++)
     {
       check=0; // checking for same value cards
       if (c1==ii) 
       {
         check++;
       }
       if (c2==ii)
       {
         check++;
       }
       if (c3==ii)
       {
         check++;
       }
       if (c4==ii)
       {
         check++;
       }
       if (c5==ii)
       {
         check++;
       }
       if (check==2) 
       {
         pair++;
       }
       else if (check==3) 
       {
         triple++;
       }
       else if (check==4) 
       {
         bomb++;
         break;
       }
     }
        String cardValue1 = "";
        String cardValue2 = "";
        String cardValue3 = "";
        String cardValue4 = "";
        String cardValue5 = "";
        String cardSpecial1 = "";
        String cardSpecial2 = "";
        String cardSpecial3 = "";
        String cardSpecial4 = "";
        String cardSpecial5 = "";
        switch (c1){ // for each randomly assigned cards, use switch statements to check for ace/king/jack/queen
          case 0:
            cardSpecial1 = "King";
              break;
          case 1:
            cardSpecial1 = "Ace";
              break;
          case 11:
            cardSpecial1 = "Jack";
              break;
           case 12:
            cardSpecial1 = "Queen";
              break;
          default:
            cardSpecial1 = ""+c1;  // if not one any of these cases, default to regular card number
        }
        switch (c2){
          case 0:
            cardSpecial2 = "King";
              break;
          case 1:
            cardSpecial2 = "Ace";
              break;
          case 11:
            cardSpecial2 = "Jack";
              break;
           case 12:
            cardSpecial2 = "Queen";
              break;
          default:
            cardSpecial2 = ""+c2;  
        }
        switch (c3){
          case 0:
            cardSpecial3 = "King";
              break;
          case 1:
            cardSpecial3 = "Ace";
              break;
          case 11:
            cardSpecial3 = "Jack";
              break;
           case 12:
            cardSpecial3 = "Queen";
              break;
          default:
            cardSpecial3 = ""+c3;  
        }
        switch (c4){
          case 0:
            cardSpecial4 = "King";
              break;
          case 1:
            cardSpecial4 = "Ace";
              break;
          case 11:
            cardSpecial4 = "Jack";
              break;
           case 12:
            cardSpecial4 = "Queen";
              break;
          default:
            cardSpecial4 = ""+c4;  
        }
        switch (c5){
          case 0:
            cardSpecial5 = "King";
              break;
          case 1:
            cardSpecial5 = "Ace";
              break;
          case 11:
            cardSpecial5 = "Jack";
              break;
           case 12:
            cardSpecial5 = "Queen";
              break;
          default:
            cardSpecial5 = ""+c5;  
        }

        if (cardNum1 >= 1 && cardNum1 <=13) // assigning cards 1-52 into shapes
        {
          cardValue1 = cardSpecial1+" of Diamonds";
        }
        else if (cardNum1 >= 14 && cardNum1 <=26){
          cardValue1 = cardSpecial1+" of Clubs";
        }
         else if (cardNum1 >= 27 && cardNum1 <=39){
          cardValue1 = cardSpecial1+" of Hearts"; 
         }
        else if (cardNum1 >= 40 && cardNum1 <=52){
          cardValue1 = cardSpecial1+" of Spades";
        }
         if (cardNum2 >= 1 && cardNum2 <=13)
        {
          cardValue2 = cardSpecial2+" of Diamonds";
        }
        else if (cardNum2 >= 14 && cardNum2 <=26){
          cardValue2 = cardSpecial2+" of Clubs";
        }
         else if (cardNum2 >= 27 && cardNum2 <=39){
          cardValue2 = cardSpecial2+" of Hearts"; 
         }
        else if (cardNum2 >= 40 && cardNum2 <=52){
          cardValue2 = cardSpecial2+" of Spades";
        }
         if (cardNum3 >= 1 && cardNum3 <=13)
        {
          cardValue3 = cardSpecial3+" of Diamonds";
        }
        else if (cardNum3 >= 14 && cardNum3 <=26){
          cardValue3 = cardSpecial3+" of Clubs";
        }
         else if (cardNum3 >= 27 && cardNum3 <=39){
          cardValue3 = cardSpecial3+" of Hearts"; 
         }
        else if (cardNum3 >= 40 && cardNum3 <=52){
          cardValue3 = cardSpecial3+" of Spades";
        }
         if (cardNum4 >= 1 && cardNum4 <=13)
        {
          cardValue4 = cardSpecial4+" of Diamonds";
        }
        else if (cardNum4 >= 14 && cardNum4 <=26){
          cardValue4 = cardSpecial4+" of Clubs";
        }
         else if (cardNum4 >= 27 && cardNum4 <=39){
          cardValue4 = cardSpecial4+" of Hearts"; 
         }
        else if (cardNum4 >= 40 && cardNum4 <=52){
          cardValue4 = cardSpecial4+" of Spades";
        }
           if (cardNum5 >= 1 && cardNum5 <=13)
        {
          cardValue5 = cardSpecial5+" of Diamonds";
        }
        else if (cardNum5 >= 14 && cardNum5 <=26){
          cardValue5 = cardSpecial5+" of Clubs";
        }
         else if (cardNum5 >= 27 && cardNum5 <=39){
          cardValue5 = cardSpecial5+" of Hearts"; 
         }
        else if (cardNum5 >= 40 && cardNum5 <=52){
          cardValue5 = cardSpecial5+" of Spades";
        }
         System.out.println(cardValue1);
        System.out.println(cardValue2);
        System.out.println(cardValue3);
        System.out.println(cardValue4);
        System.out.println(cardValue5);
        System.out.println();
        //else if (card)
        if (triple==1)
        {
          if (pair==1)
          {
            fullHouse++;
          }
          else 
          {
            tripleF++;
          }
        }
        else if (bomb==1)
        {
          fourF++;  //four of a kind increases by 1
        }
        else if (pair ==2)
        {
          twoPairs++; //two pair increases by1
        }
        else if (pair ==1)
        {
          pairF++; // one pair increases by 1
        }

      }
    System.out.println("Full House: " + fullHouse); //prints counters
    System.out.println("Three-of-a-kind: " + tripleF);
    System.out.println("Four-of-a-kind: " + fourF);
    System.out.println("Two Pair: " + twoPairs);
    System.out.println("One Pair: " + pairF);
    System.out.println();
    double genT=(double) genTimes; // probabilities of each hand pattern
    double prob1 = (double)fullHouse/genT;
    double prob2 = (double)tripleF/genT;
    double prob3 = (double)fourF/genT;
    double prob4 = (double)twoPairs/genT;
    double prob5 = (double)pairF/genT;
    
    System.out.printf("The probability of Full House is : %.3f \n", prob1); // prints probability
    System.out.printf("The probability of Three-of-a-kind is : %.3f \n", prob2);
    System.out.printf("The probability of Four-of-a-kind is : %.3f \n", prob3);
    System.out.printf("The probability of Two Pair is : %.3f \n", prob4);
    System.out.printf("The probability of One Pair is : %.3f \n", prob5);
  }
}