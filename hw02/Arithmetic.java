///// Esther Park
// CSE 002-310
// HW02
//// Arthmetic Calculations
public class Arithmetic{
  // essential to every program
  public static void main (String[] args){
    // number of pairs of pants
    int numPants = 3; 
    // Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    // Cost per shirt
    double ShirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    // cost per belt
    double beltCost = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    //Total price of pants
    double costPants = (int) ((numPants * pantsPrice) * 100);
    //converts to 2 decimal places
    costPants = costPants / 100; 
    
    //Total price of Sweatshirts
    double costShirts = (int) ((numShirts * ShirtPrice) * 100);
    //converts to 2 decimal places
    costShirts = costShirts / 100;
    
    //Total price of belts
    double costBelts = (int) ((numBelts * beltCost) * 100);
    //converts to 2 decimal places
    
    //Total tax on pants
    double taxPants = (int) ((costPants * paSalesTax) * 100);
    //converts to 2 decimal places
    taxPants = taxPants / 100;
    
    //Total tax on sweatshirts
    double taxShirts = (int) ((costShirts * paSalesTax) * 100);
    //converts to 2 decimal places
    taxShirts = taxShirts / 100;
    
    //Total tax on belts
    double taxBelt = (int) ((beltCost * paSalesTax) * 100);
    //converts to 2 decimal places
    taxBelt = taxBelt / 100;
    
    //Subtotal before tax
    double subTotal = costPants + costShirts + beltCost;
    
    //total tax collected
    double TotalTax = taxPants + taxShirts + taxBelt;
    //grand total of transaction
    double grandTotal = subTotal + TotalTax;
    
    //prints cost of pants
    System.out.println ("Cost of Pants: $" + costPants);
    //prints cost of sweatshirts
    System.out.println ("Cost of Sweatshirts: $" + costShirts);
    //prints cost of belt
    System.out.println ("Cost of Belt: $" + beltCost);
    //prints tax from pants
    System.out.println ("Tax on Pants: $" + taxPants);
    //prints tax from sweatshirts
    System.out.println ("Tax on Sweatshirts: $" + taxShirts);
    //prints tax from belt
    System.out.println ("Tax on Belt:$" + taxBelt);
    
    
    //prints subtotal
    System.out.println ("Subtotal: $" + subTotal);
    //prints total tax
    System.out.println ("Total Tax: $" + TotalTax);
    //prints grand total
    System.out.println ("Grand Total: $" + grandTotal);
    
    
      
    
    
  }
}